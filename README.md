# LDIF Examiner
This is a small script that examines an LDIF file and shows possibly interesting data to help you win your CTF:

* Non-default objects.
* Only the interesting properties for each object, along with highlighted unknown properties to make hints stand out easily.
* Tips on interesting leads found in the LDIF (e.g. suspected presence of Micorosoft Exchange or suspected presence of service accounts).

**Note:** This is not a bloodhound replacement, and some objects might be filtered even if they have some significance.
This is merely a way to get initial data on a domain to obtain some leads by easily seeing irregularities.

This script can also be used as a module (also see [ldifparser.py](#ldifparserpy)):
```python
from ldifparser import LdifParser
from ldifexaminer import LdifExaminer

with open('myfile.ldif') as r:
	p = LdifParser(p)
	e = LdifExaminer(p.ldif, 'config.ini')

	tips, tree = e.tips, e.tree
	print (e.tips)
	print (e.tree)

	# or
	for obj in e.results['tree'].items():
		for prop, value in obj.items():
			print(f'{prop} => {value}')
		print('\n')

```

### configuration
The configuration for the examiner is a file with the following options and structure:
```
[properties]
	; Properties that should not be displayed in the final output
	ignore-properties :
		ref
		msDS-*
		...

	; Properties that we expect to see.
	; they will be displayed in the final output if present on the object, but not highlighted.
	expected-properties :
		dn
		memberOf
		displayName
		...

; If one of the object properties matches one of the values listed here,
; the object will be dismissed from the final output.
[ignore-objects]
	prop1 :
		value-to-dismiss
		*dismiss

	dn :
		*CN=Builtin,DC=*
		CN=krbtgt,CN=Users,DC=*

	; All objects will the ignoreMe property set will be dismissed
	ignoreMe : *

; Tips are presented according to properties that match the following values, in a similar fashion
; to the ignore-objects section above. Tips will be based on all objects and properties, including
; those that were dismissed from the final output.
; Note that the format isn't a standard ini, and each of the values contains the tip text.
[tips]
	prop1 : 
		*special* : A special prop found

	dn :
		CN=*svc*,OU=* : Possible service accounts found
```


## ldifparser.py

A small script that parses an LDIF file. Can be used as a script that exports the LDIF to a usable format, or as a module:
```python
from ldifparser import LdifParser

with open('myfile.ldif') as r:
	p = LdifParser(p)
	for obj in p.ldif:
		for prop, value in obj.items():
			print(f'{prop} => {value}')
		print('\n')

	# or
	p.export('output_file.json', 'json')
```