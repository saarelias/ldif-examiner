#!/usr/bin/env python3

import os
import fnmatch
import logging
import argparse
import configparser

from collections import Counter
from ldifparser import LdifParser
from colorama import Fore, Style, init as colorinit

class LdifExaminerConfigParser(configparser.ConfigParser):
    def get_list(self, section, option):
        value = self.get(section, option)
        return list(filter(None, (x.strip() for x in value.splitlines())))
    def get_section_list(self, section, converter=None):
        value = self.options(section)
        converter = converter or (lambda x: x)
        res = []
        for option in value:
            res.append((option, converter(self.get_list(section, option))))
        return res

class LdifExaminer:
    def __init__(self, ldif_iterable, config_file_path, colored=True):
        """
            ldif_iterable: iterable
                The LDIF represented as an iterable
            config_file_path: str
                The examiner configuration file
            colored: bool, optional
                Should the output be colored (default True)
        """
        self.ldif = ldif_iterable
        self.color_output = colored

        self.results = {'tree': [], 'tips': set()}
        self.counters = Counter()

        self.logger = logging.getLogger('LdifExaminer')

        self.config = LdifExaminerConfigParser(delimiters=(':'))
        self.config.read(config_file_path)

        self.examine()

    def examine(self):
        ignore_objects = dict(self.config.get_section_list('ignore-objects'))
        ignore_properties = self.config.get_list('properties', 'ignore-properties')
        tips_conditions = dict(self.config.get_section_list('tips', LdifExaminer.split_config_list))

        for obj in self.ldif:
            self.counters['objects'] += 1

            ignore = False
            reduced_object = {}

            for prop, values_list in obj.items():
                for value in values_list:
                    # Check for tips
                    filter_result, filter_expression = self.should_filter(value, tips_conditions.get(prop.lower(), []), lambda rule: rule[0])
                    if filter_result:
                        self.logger.debug('Tip "%s" matches prop %s of %s: %s', filter_expression[0], prop, self.represent_object(obj), value)
                        self.results['tips'].add(filter_expression[1])

                    # The outer loop must continue to find tips even if the object itself is deemed irrelevant,
                    # so just check if we already decided to ignore this object
                    if not ignore:
                        # Check if object should be dismissed
                        filter_result, filter_expression = self.should_filter(value, ignore_objects.get(prop.lower(), []))
                        if filter_result:
                            self.logger.debug('Object %s matches ignore-objects prop %s: %s', self.represent_object(obj), prop, value)
                            ignore = True

                        # Check if property should be dismissed
                        filter_result, filter_expression = self.should_filter(prop, ignore_properties)
                        if filter_result:
                            self.logger.debug('Property %s matches ignore-properties rule %s', prop, filter_expression)
                        else:
                            reduced_object[prop] = reduced_object.get(prop, []) + [value]


            if not ignore and reduced_object:
                self.counters['reducedObjects'] += 1
                self.results['tree'].append(reduced_object)
            else:
                self.logger.debug('Skipping object %s', self.represent_object(obj))

        total_tips = len(self.results['tips'])
        if total_tips:
            self.counters['tips'] = total_tips

    def should_filter(self, value, expressions_list, modifier=lambda x: x):
        for expression in expressions_list:
            exp = modifier(expression)
            if fnmatch.fnmatch(value.lower(), exp.lower()):
                return True, expression
        return False, None

    def represent_object(self, obj):
        k = [*obj]
        if len(k) == 0:
            return '<empty>'
        elif len(k) == 1 and 'dn' not in k:
            return k[0]
        return obj.get('dn', '<unknown>')

    @property
    def tree(self):
        output_list = []
        for obj in self.results['tree']:
            obj_output_list = []

            for item in obj.items():
                output_format = '{:>25} {}'

                if item[0] not in self.config.get_list('properties', 'expected-properties'):
                    self.counters['interestingProps'] += 1

                    if self.color_output:
                        output_format = Fore.YELLOW + output_format + Style.RESET_ALL
                for value in item[1]:
                    obj_output_list.append(output_format.format(item[0], value))

            output_list.append('\n'.join(obj_output_list))

        return  ('-' * 60) + '\n' + \
                ('\n' + ('-' * 60) + '\n').join(output_list) + \
                '\n' + ('-' * 60)

    @property
    def tips(self):
        tips_list = '\n'.join(self.results['tips'])

        # This is so colors won't make empty tips string not empty.
        if not tips_list:
            return ''

        if self.color_output:
            return Fore.GREEN + tips_list + Style.RESET_ALL
        return tips_list

    @staticmethod
    def split_config_list(config_list):
        # ['prop : value'] => [['prop', 'value']]
        return [*map(lambda x: [*map(str.strip, x.rsplit(':'))], config_list)]

def main(args):
    with open(args.file) as ldif_file:
        p = LdifParser(ldif_file)

    e = LdifExaminer(p.ldif, args.config, not args.no_color)

    tips, tree = e.tips, e.tree

    summary = 'Found {c}{reducedObjects}{ec} possibly interesting objects (out of {objects})'
    if 'interestingProps' in e.counters:
        summary += ', {c}{interestingProps}{ec} properties that might include useful information'
    if 'tips' in e.counters:
        summary += ', and {c}{tips}{ec} tips'.format(c=Fore.CYAN, ec=Style.RESET_ALL, **e.counters)
    summary = summary.format(c='' if args.no_color else Fore.CYAN, ec='' if args.no_color else Style.RESET_ALL, **e.counters)

    if args.output_file:
        with open(args.output_file, 'w') as o:
            o.write(tree)
            if tips:
                o.write('\n' + tips)
            o.write()
    else:
        print (tree)
        if tips:
            print (tips)
        print (summary)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parses an LDIF file and displayes possibly interesting data')
    parser.add_argument('file', help='The LDIF file to examine')
    parser.add_argument('-c', '--config', help='The examiner config file', default=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'examiner-config.ini'))
    parser.add_argument('-o', '--output-file', help='Output file to write to. Leave blank for stdout.')
    parser.add_argument('-d', '--debug-log', help='Debug log file to write to. Leave blank for no debug log.')
    parser.add_argument('--no-color', help='Disable color output (-o implicitly turns this option on).')
    args = parser.parse_args()

    assert os.path.isfile(args.file), 'Input file does not seem to exist'
    assert os.path.isfile(args.config), 'Config file does not seem to exist'

    args.no_color = args.no_color or args.output_file

    colorinit()

    log_format = '[%(levelname)s] %(name)s: %(message)s'
    logging.basicConfig(
        level=logging.DEBUG if args.debug_log else logging.INFO,
        format=log_format,
        filename=args.debug_log
    )

    # No need for the extra console output if no debug_log is set
    if args.debug_log:
        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(logging.Formatter(log_format))
        streamHandler.setLevel(logging.INFO)
        logging.getLogger('').addHandler(streamHandler)

    main(args)
