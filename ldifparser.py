#!/usr/bin/env python3

import os
import re
import io
import json
import logging
import argparse

class LdifParser:
    LDIF_COMMENT = '#'
    LDIF_WRAP = ' '
    LDIF_LINE_RE = re.compile('^([A-z0-9-]+)::?\s(.+)$') # Two colons indicate base64 value
    EXPORT_FORMATS = ('json',)

    def __init__(self, ldif_content):
        """
            ldif_content: str/list/tuple/file
                The LDIF content represented as a list, a tuple or a file-like object.
                If a string is passed, it is considered a file path to be read.
        """

        assert isinstance(ldif_content, (list, tuple, str, io.IOBase)), 'Invalid ldif_content type specified'

        self.last_property = ()
        self.current_object = {}
        self.ldif = []
        self.logger = logging.getLogger('LdifParser')

        try:
            file_opened = False
            if isinstance(ldif_content, str):
                ldif_content = open(ldif_content, 'r')
                file_opened = True

            self.parse(ldif_content)

        except FileNotFoundError:
            self.logger.fatal('Seems like a file path was provided, but %s was not found', ldif_content)
            raise

        finally:
            if file_opened:
                ldif_content.close()

    def parse(self, ldif_content):
        line_num = 0
        for line in ldif_content:
            line_num += 1
            line = line.rstrip('\r\n')

            if line.startswith(self.LDIF_COMMENT):
                self.logger.debug('[L%d] Comment left out: %s', line_num, line)

            elif line.startswith(self.LDIF_WRAP):
                if not self.last_property:
                    self.logger.debug('[L%d] Illogical wrapping encountered: %s', line_num, line)
                    continue

                self.last_property = (self.last_property[0], self.last_property[1] + line[1:])

            elif not line.strip():
                self.logger.debug('[L%d] Empty line - assuming end of object', line_num)
                self.addCurrentObjectToTree()

            else:
                match = re.match(self.LDIF_LINE_RE, line)
                if not match:
                    self.logger.warning('[L%d] Unparsable line: %s', line_num, line)
                    continue

                self.addCurrentPropertyToObject()

                self.last_property = match.groups()
                self.logger.debug('[L%d] Found property %s', line_num, self.last_property[0])

        self.addCurrentObjectToTree()

    def addCurrentPropertyToObject(self):
        if self.last_property:
            self.current_object[self.last_property[0]] = self.current_object.get(self.last_property[0], []) + [self.last_property[1]]
            self.logger.debug('Property %s added to object', self.last_property[0])
            self.last_property = ()

    def addCurrentObjectToTree(self):
        self.addCurrentPropertyToObject()

        if self.current_object:
            self.ldif.append(self.current_object)
            self.logger.debug('Object added to tree')
            self.current_object = {}

    def export(self, output_file, export_format='json'):
        result = None

        if export_format == 'json':
          result = json.dumps(self.ldif)

        with open(output_file, 'w') as f:
          f.write(result)

def main(args):
    with open(args.file) as ldif_file:
        p = LdifParser(ldif_file)
        p.export(args.output_file, args.format)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse an LDIF file to a list of dictionaries representing objects')
    parser.add_argument('file', help='The LDIF file to parse')
    parser.add_argument('-o', '--output-file', help='Output file to write to. Defaults to ldif.[format]')
    parser.add_argument('-f', '--format', help='The export format.', default='json', choices=LdifParser.EXPORT_FORMATS)
    parser.add_argument('-d', '--debug-log', help='Debug log file to write to. Leave blank for no debug log.')
    args = parser.parse_args()

    assert os.path.isfile(args.file), 'Input file does not seem to exist'
    args.output_file = args.output_file or os.path.basename(args.file) + '.' + args.format

    log_format = '[%(levelname)s] %(name)s: %(message)s'
    logging.basicConfig(
        level=logging.DEBUG if args.debug_log else logging.INFO,
        format=log_format,
        filename=args.debug_log
    )

    # No need for the extra console output if no debug_log is set
    if args.debug_log:
        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(logging.Formatter(log_format))
        streamHandler.setLevel(logging.INFO)
        logging.getLogger('').addHandler(streamHandler)

    main(args)
